<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.2.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
  </parent>
  <groupId>com.arquitecturajava</groupId>
  <artifactId>servidor</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <name>servidor</name>
  <description>Demo project for Spring Boot</description>

  <properties>
    <java.version>1.8</java.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-webflux</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>org.junit.vintage</groupId>
          <artifactId>junit-vintage-engine</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>io.projectreactor</groupId>
      <artifactId>reactor-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.arquitecturajava</groupId>
      <artifactId>dominio</artifactId>
      <version>0.0.1-SNAPSHOT</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

</project>



@RestController

package com.arquitecturajava.servidor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arquitecturajava.dominio.Factura;

@RestController
public class FacturasController {

  @GetMapping("/facturas")
  public List<Factura> buscarTodas() {
    
    
    List<Factura> lista= new ArrayList<Factura>();
    lista.add(new Factura(1,"ordenador",200));
    lista.add(new Factura(2,"tablet",300));
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      
      e.printStackTrace();
    }
    return lista;
    
    
  }
}




package com.arquitecturajava.servidor;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arquitecturajava.dominio.Factura;

import reactor.core.publisher.Flux;

@RestController
public class FacturasController {

  @GetMapping("/facturas")
  public Flux<Factura> buscarTodas() {
    
    
Flux<Factura> lista= Flux.just(new Factura(1,"ordenador",200)
,new Factura(2,"tablet",300)).delaySequence(Duration.ofSeconds(3));
    
    return lista;
  }
}




package com.arquitecturajava.dominio;

public class Factura {

  private int numero;
  private String concepto;
  private double importe;
  public int getNumero() {
    return numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public String getConcepto() {
    return concepto;
  }
  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }
  public double getImporte() {
    return importe;
  }
  public void setImporte(double importe) {
    this.importe = importe;
  }
  public Factura(int numero, String concepto, double importe) {
    super();
    this.numero = numero;
    this.concepto = concepto;
    this.importe = importe;
  }
  public Factura() {
    super();
  }
  
  
}


package com.arquitecturajava.cliente;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.arquitecturajava.dominio.Factura;
@Service
public class ClienteFacturasService {

  public List<Factura> buscarTodas() {

    RestTemplate plantilla = new RestTemplate();
    Factura[] facturas = plantilla.getForObject("http://localhost:8080/facturas", Factura[].class);
    List<Factura> lista = Arrays.asList(facturas);
    return lista;

  }
}


package com.arquitecturajava.cliente;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.arquitecturajava.dominio.Factura;

@Controller
public class FacturaClienteController {

  @Autowired
  ClienteFacturasService servicio;

  @RequestMapping("/lista")
  public String lista(Model modelo) {
    
    List<Factura> lista= new ArrayList<Factura>();
    lista.addAll(servicio.buscarTodas());
    lista.addAll(servicio.buscarTodas());
    lista.addAll(servicio.buscarTodas());
    
    modelo.addAttribute("lista", lista);
    return "lista";
  }
}


Angular


<table>
    <thead>
        <tr>
            <th> id </th>
            <th> concepto </th>
               <th> importe </th>
        </tr>
    </thead>
    <tbody>
   
        <tr th:each="factura : ${lista}">
            <td><span th:text="${factura.numero}"> </span></td>
            <td><span th:text="${factura.concepto}"></span></td>
              <td><span th:text="${factura.importe}"></span></td>
        </tr>
    </tbody>
</table>




import org.springframework.web.reactive.function.client.WebClient;

import com.arquitecturajava.dominio.Factura;

import reactor.core.publisher.Flux;
@Service
public class ClienteFacturasService {

  public Flux<Factura> buscarTodas() {

  
    WebClient cliente = WebClient.create("http://localhost:8080/facturas");
    Flux<Factura> facturas=cliente.get().retrieve().bodyToFlux(Factura.class);
    Flux<Factura> facturas2=cliente.get().retrieve().bodyToFlux(Factura.class);
    Flux<Factura> facturas3=cliente.get().retrieve().bodyToFlux(Factura.class);
    Flux<Factura> todas=Flux.merge(facturas,facturas2,facturas3);
    System.out.println(todas);
    return todas;
  

  }
}

